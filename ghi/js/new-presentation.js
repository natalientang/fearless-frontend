// Function executes when page loads
// Make the function asyncrhonous to let the code know we are waiting for data in some places
window.addEventListener('DOMContentLoaded', async () => {
    // URL is needed to access a database filled with presentations
    const url = 'http://localhost:8000/api/conferences/'

    try {
        const response = await fetch(url);
        // There's no data retrieved from url, then enter if statement
        // !response.ok => response.ok == false
        if (!response.ok) {
            console.error("there has been an error")
        } else {
            // Converting response to readable data from HTML to JSON
            const data = await response.json();
            //  Referencing an element based on the id in the HTML (Ref: line 70, new-presentation.html)
            const selectTag = document.getElementById('conferences');
            // Creating option elements and adding them into the select element (Ref: line 71, new-presentation.html)
            for (let conference of data.conferences) {
                // Creating a new option element
                let option = document.createElement('option');
                option.value = conference.id;
                option.innerHTML = conference.name;
                // Adding options to the select element
                selectTag.appendChild(option);
            }

            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                // Retrieving data from the HTML form
                const formData = new FormData(formTag);
                const conferenceid = Object.fromEntries(formData).conferences;
                const presenter_name = Object.fromEntries(formData).presenter_name;
                const presenter_email = Object.fromEntries(formData).presenter_email;
                const synopsis = Object.fromEntries(formData).synopsis;
                const company_name = Object.fromEntries(formData).company_name;
                const title = Object.fromEntries(formData).title;
                // Creates a new object
                let test = {}
                test["presenter_name"] = presenter_name
                test["presenter_email"] = presenter_email
                test["synopsis"] = synopsis
                test["company_name"] = company_name
                test["title"] = title
                // Converts the object to a string
                let json = JSON.stringify(test)
                // Link to see the specific presentation
                const presentationsUrl = `http://localhost:8000/api/conferences/${conferenceid}/presentations/`;
                const fetchConfig = {
                    method: "post", // Creates a presentation
                    body: json, // Creates based on the data inside the body
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                // Fetches the presentation
                const response = await fetch(presentationsUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();
                }
            });
        }
    } catch (e) {
        console.error("There has been an error")
    }
});
